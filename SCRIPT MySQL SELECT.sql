use labor_sql;
SELECT * FROM PC;
SELECT * FROM PRODUCT;
SELECT * FROM LAPTOP;
SELECT * FROM PRINTER;
-- TASK 1:
-- SELECT maker m, type t FROM product ORDER BY maker;--
-- SELECT model, ram, screen FROM laptop WHERE price >= 1000;--
-- SELECT model, ram, screen, price FROM laptop ORDER BY ram ASC, price DESC; --
-- SELECT code, model, color, type, price FROM printer WHERE color = 'y' ORDER BY price DESC;
-- SELECT model, speed, hd FROM pc WHERE cd = '12x' OR cd = '24x' AND price < 600;
-- SELECT model, speed, hd, cd, price FROM pc WHERE cd = '12x' OR cd = '24x' AND price < 600 ORDER BY speed DESC;
-- SELECT name, class FROM ships WHERE class = 'Iowa' OR CLASS = 'Kongo' OR CLASS = 'North Carolina' OR 'Renown' OR CLASS = 'Revenge' OR CLASS = 'Tennessee' OR CLASS = 'Yamato'ORDER BY name;
-- SELECT * FROM pc WHERE speed >= 500 AND price < 800 ORDER BY price DESC;
-- SELECT * FROM printer WHERE type != 'Matrix' AND price < 300 ORDER BY type DESC;
-- SELECT model, speed FROM pc WHERE price BETWEEN 400 AND 600 ORDER BY hd;
-- SELECT p.model, p.speed, p.hd FROM pc p LEFT JOIN product p1 ON p.model=p1.model WHERE hd BETWEEN 10 AND 20 AND maker = 'A' ORDER BY speed;
-- SELECT model, speed, hd, price FROM laptop WHERE screen >= 12 ORDER BY price DESC;
-- SELECT model, type, price FROM printer WHERE price < 300 ORDER BY type DESC;
-- SELECT model, ram, price FROM laptop WHERE ram = 64 ORDER BY screen;
-- SELECT model, ram, price FROM laptop WHERE ram > 64 ORDER BY hd;
-- SELECT model, speed, price FROM pc WHERE speed BETWEEN 500 AND 750 ORDER BY hd DESC;
-- SELECT point, date, `out`FROM Outcome_o WHERE `out` > 2000  ORDER BY date DESC;
-- SELECT * FROM Income_o WHERE inc BETWEEN 5000 AND 10000 ORDER BY inc;
-- SELECT * FROM Income WHERE point = 1 ORDER BY inc;
-- SELECT * FROM Outcome  WHERE point = 2 ORDER BY `out`;
-- NULLs??? SELECT * FROM  classes WHERE country = 'Japan' /*AND class != 'null' AND type != 'null'  AND country != 'null' AND numGuns != 'null' AND bore != 'null' AND displacement != 'null' */ ORDER BY type DESC;
-- SELECT name, launched FROM ships WHERE launched  BETWEEN 1920 AND 1942 ORDER BY launched DESC;
-- SELECT ship, battle, result FROM outcomes WHERE battle = 'Guadalcanal' AND result = 'OK' ORDER BY ship DESC;
-- SELECT ship, battle, result FROM outcomes WHERE result = 'SUNK' ORDER BY ship DESC;
-- SELECT class, displacement FROM classes WHERE displacement >= 40000 ORDER BY type;
-- SELECT trip_no, town_from, town_to FROM trip WHERE town_from = 'London' OR town_to = 'London' ORDER BY time_out;
-- SELECT trip_no , plane , town_from , town_to FROM trip WHERE plane = 'TU-134' ORDER BY time_out DESC;
-- SELECT trip_no , plane , town_from , town_to FROM trip WHERE  plane = 'IL-86' ORDER BY plane;
-- SELECT trip_no , town_from , town_to FROM trip WHERE town_from != 'Rostov' AND town_to != 'Rostov' ORDER BY plane;
-- TASK 2:
-- ? SELECT model FROM pc WHERE model RLIKE '(?=.*?[1].*?[1])';
-- ? SELECT * FROM Outcome WHERE 'date' RLIKE '^(20)\d\d([- /.])(03)\2(0[1-9]|[12][0-9]|3[01])$';
-- ? SELECT * FROM Outcome_o WHERE date RLIKE '^(20)\d\d([- /.])(0[1-9]|1[012])\2(14)$'; 
-- SELECT name FROM Ships WHERE name RLIKE '^((W)(.*)(n))$'
-- SELECT name FROM Ships WHERE name RLIKE '^((.*)(E){2}(.*))$'
-- SELECT name, launched FROM Ships WHERE name RLIKE '.*(?<!a)$'
-- ? SELECT name FROM battles WHERE name RLIKE  '\b(.+)\b[[:blank:]]\b(?<!c)\b'   /*'^(.*)$^(?<!c)$'*/
-- ...
-- TASK 3:
-- SELECT maker , type , speed , hd FROM pc, product  WHERE hd <= 8;
-- SELECT maker FROM pc, product  WHERE speed <= 500;
-- SELECT maker FROM laptop, product  WHERE speed <= 500;
-- SELECT L1.model Model1, L2.Model Model2, L1.hd hd1, L2.hd hd2, L1.ram ram1, L2.ram ram2 FROM Laptop L1, Laptop L2 WHERE L1.hd = L2.hd AND L1.ram = L2.ram ORDER BY model1 DESC;
-- ??? SELECT C1.type Type1, C2.type Type2, C1.country Country1, C2.country Country2, FROM Classes C1, Classes C2 WHERE C1.type = bb, AND C2.type = bc;
-- SELECT p.model, p1.maker, p.price FROM pc p LEFT JOIN product p1 ON p.model=p1.model WHERE PRICE < 600;
-- SELECT p.model, p1.maker, p.price FROM printer p LEFT JOIN product p1 ON p.model=p1.model WHERE PRICE > 300;
-- SELECT p1.maker, p.model, p.price FROM pc p LEFT JOIN product p1 ON p.model=p1.model;
-- SELECT p.maker, l.model, p.type, l.speed FROM laptop l LEFT JOIN product p ON l.model=p.model WHERE speed > 600;
-- SELECT displacement FROM Ships S LEFT JOIN Classes C ON S.class = C.class;
-- ? SELECT B.name Name, B.date Date FROM Outcomes O RIGHT JOIN Battles B ON O.ship = B.date WHERE O.result = 'OK';
-- ? SELECT name, Id_psg FROM Passenger, Pass_in_trip;

-- Task 4:
-- SELECT maker FROM Product WHERE type IN (SELECT type FROM Product WHERE type = 'PC');
-- SELECT maker FROM Product WHERE type = ALL (SELECT type FROM Product WHERE type = 'PC');
-- SELECT maker FROM Product WHERE type = ANY (SELECT type FROM Product WHERE type = 'PC'); 
-- SELECT maker FROM Product WHERE type IN (SELECT type FROM Product WHERE type = 'PC' OR type = 'Laptop'); 
-- SELECT maker FROM Product WHERE type <= All (SELECT type FROM Product WHERE type = 'PC'); 
-- Task 5:
-- SELECT maker FROM Product P WHERE EXISTS (SELECT * FROM PC WHERE P.model=model);
-- SELECT maker FROM Product P WHERE EXISTS (SELECT * FROM PC WHERE P.model = model AND speed >= 750);
-- ?SELECT maker FROM Product P WHERE EXISTS (SELECT * FROM PC, Laptop  WHERE P.model = model AND speed >= 750);
-- SELECT maker FROM Product P WHERE EXISTS (SELECT * FROM Printer P1, PC P2 WHERE P.model = P2.model AND speed = 900);
-- ?SELECT maker FROM Product P WHERE EXISTS (SELECT * FROM Printer P1, Laptop L WHERE P.model = L.model OR P.model = P1.model);
-- Task 6:
-- SELECT AVG(Price) 'середня ціна =' FROM Laptop;
-- SELECT CONCAT('код=', code), CONCAT('модель=', model), CONCAT('частота процесора=', speed), CONCAT('оперативна память=', ram), CONCAT('память=', hd), CONCAT('cd-привід=', cd), CONCAT('ціна=', price) FROM pc;
-- SELECT DATE_FORMAT(date,  '%Y.%m.%d'  ) FROM  Income;
-- SELECT ship, battle, CASE result WHEN 'sunk' THEN 'потоплений' WHEN 'damaged' THEN 'пошкоджений' ELSE 'цілий'END AS result FROM  Outcomes;
-- SELECT CONCAT('from ', town_from, ' to ', town_to)FROM Trip;
-- Task 7:
-- SELECT model, MAX(price) max_price FROM Printer; -- SELECT model, price FROM Printer WHERE price > 300 GROUP BY model;
-- ?v.1) SELECT type FROM Product P LEFT JOIN ON  L.model=P1.model (SELECT model, speed FROM  Laptop L UNION SELECT  model, speed FROM PC P1) ;
-- ?v.2) SELECT type, model, speed FROM Product P LEFT JOIN (SELECT model, speed FROM Laptop L UNION SELECT  model, speed FROM PC P1) ON P.model=P1.model AND P.model=L.model;
-- ?v.3) SELECT type, speed FROM Product P LEFT JOIN Laptop L ON P.model=L.model LEFT JOIN PC P1 ON P.model=P1.model AND L.model=P1.model WHERE L.speed >= P1.speed;
-- SELECT maker, price FROM Product P LEFT JOIN Printer P1 ON P.model=P1.model WHERE price < 200;
-- ?SELECT P1.maker FROM Product P1 WHERE EXISTS (SELECT P2.maker FROM Product P2 WHERE P2.type = 'PC') GROUP BY maker;
-- ?SELECT COUNT(*) count, AVG(hd) hd_middle FROM PC P WHERE EXISTS (SELECT maker FROM Product P1 WHERE type = 'Printer');
-- Task 8:





SELECT * FROM pc;
SELECT * FROM Product;






































